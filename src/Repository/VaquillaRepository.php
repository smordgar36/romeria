<?php

namespace App\Repository;

use App\Entity\Vaquilla;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Vaquilla|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vaquilla|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vaquilla[]    findAll()
 * @method Vaquilla[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VaquillaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Vaquilla::class);
    }

//    /**
//     * @return Vaquillas[] Returns an array of Vaquillas objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Vaquillas
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
