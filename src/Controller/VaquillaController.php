<?php

namespace App\Controller;

use App\Entity\Vaquilla;
use App\Form\VaquillaType;
use App\Repository\VaquillaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/vaquilla")
 */
class VaquillaController extends AbstractController
{
    /**
     * @Route("/", name="vaquilla_index", methods="GET")
     */
    public function index(VaquillaRepository $vaquillaRepository): Response
    {
        return $this->render('vaquilla/index.html.twig', ['vaquilla' => $vaquillaRepository->findAll()]);
    }

    /**
     * @Route("/new", name="vaquilla_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $vaquilla = new Vaquilla();
        $form = $this->createForm(VaquillaType::class, $vaquilla);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($vaquilla);
            $em->flush();

            return $this->redirectToRoute('vaquilla_index');
        }

        return $this->render('vaquilla/new.html.twig', [
            'vaquilla' => $vaquilla,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="vaquilla_show", methods="GET")
     */
    public function show(Vaquilla $vaquilla): Response
    {
        return $this->render('vaquilla/show.html.twig', ['vaquilla' => $vaquilla]);
    }

    /**
     * @Route("/{id}/edit", name="vaquilla_edit", methods="GET|POST")
     */
    public function edit(Request $request, Vaquilla $vaquilla): Response
    {
        $form = $this->createForm(VaquillaType::class, $vaquilla);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('vaquilla_edit', ['id' => $vaquilla->getId()]);
        }

        return $this->render('vaquilla/edit.html.twig', [
            'vaquilla' => $vaquilla,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="vaquilla_delete", methods="DELETE")
     */
    public function delete(Request $request, Vaquilla $vaquilla): Response
    {
        if ($this->isCsrfTokenValid('delete'.$vaquilla->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($vaquilla);
            $em->flush();
        }

        return $this->redirectToRoute('vaquilla_index');
    }
}
