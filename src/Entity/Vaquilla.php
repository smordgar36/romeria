<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VaquillaRepository")
 */
class Vaquilla
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $plaza;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlaza(): ?string
    {
        return $this->plaza;
    }

    public function setPlaza(string $plaza): self
    {
        $this->plaza = $plaza;

        return $this;
    }
}
